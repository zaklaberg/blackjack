class Blackjack {
    constructor() {
        this.dealer = {
            cards: []
        }

        this.player = {
            cards: []
        }

        this.playerDraw = this.draw.bind(this, this.player)
        this.dealerDraw = this.draw.bind(this, this.dealer)
    }
    async init() {
        const GAME_URL = 'https://deckofcardsapi.com/api/deck/new/draw/?count=4'
        const resp = await fetch(GAME_URL)
        const res = await resp.json()
        const cards = this.customizeCards(res.cards)

        // Store initial hand
        this.deckId = res.deck_id
        this.dealer.cards.push(...cards.slice(0, 2))
        this.dealer.cards[0].is_visible = false
        this.player.cards.push(...cards.slice(2))
    }

    evaluateWinner(dealerTotal, playerTotal) {
        if (playerTotal > 21) {
            return -1
        }
        else if (dealerTotal > 21 && playerTotal < 21) {
            return 1
        }
        else if (dealerTotal > playerTotal && dealerTotal <= 21) {
            return -1
        }
        else if (dealerTotal < playerTotal && playerTotal <= 21) {
            return 1
        }
        else if (dealerTotal === playerTotal) {
            return 0
        }
        else {
            return 0
        }
    }

    customizeCards(cards) {
        cards.forEach(card => {
            card.is_visible = true
            
            // Remap values to actual numbers
            const named = ['KING', 'QUEEN', 'JACK']
            const namedValue = 10
            if (named.includes(card.value)) {
                card.userValue = namedValue
            }

            // default to value = 1 for ace
            else if (card.value === 'ACE') {
                card.userValue = 1
            }
            else {
                card.userValue = card.value
            }
        })
        return cards
    }

    totalValue(cards) {
        const total = cards.reduce((acc, card) => acc + parseInt(card.userValue,10), 0)
        return total
    }

    async draw(who) {
        const card = await this.drawCards(1)
        who.cards.push(card)
    }

    async drawCards(cardsToDraw) {
        console.log(this.deckId, cardsToDraw)
        const resp = await fetch(`https://deckofcardsapi.com/api/deck/${this.deckId}/draw/?count=${cardsToDraw}`)
        const res = await resp.json()
        return this.customizeCards(res.cards)
    }
}

export async function buildBlackjack() {
    // unfortunate abbreviation
    const bj = new Blackjack()
    await bj.init()
    return bj
}