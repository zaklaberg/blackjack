import VueRouter from 'vue-router'
import Start from './components/Start.vue'
import Play from './components/Play.vue'

const routes = [{
    path: '/',
    name: 'Start',
    component: Start
},
{
    path: '/Start',
    redirect: '/'
},
{
    path: '/Play/:playerName',
    name: 'Play',
    component: Play
}
]

const router = new VueRouter({ routes })

export default router;